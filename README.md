# templates



## Description
Collection of template files. `jinja2` templates can be `curl`ed or downloaded with `wget` (or some equivalent tool).

## Installation

### Requirements

- `python3`
- (Optional) `pdm` package manager
- (Optional) `pipx`
- (Optional) `cookiecutter`: **Required** for templates in `cookiecutter/`

## Usage

### Cookiecutter templates

Install `cookiecutter` (i.e. `$ pipx install cookiecutter`), then run the following command (subsituting `$template_dir` with the path to the `cookiecutter` template):

`$ cookiecutter https://gitlab.com/redjax/templates --directory="cookiecutter/$template_dir .`
