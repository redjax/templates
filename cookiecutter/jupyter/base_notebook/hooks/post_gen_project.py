import subprocess
from pathlib import Path
import shutil
import os
from typing import Any

from dataclasses import dataclass, field

repo_name: str = "{{cookiecutter.repo_name}}"


@dataclass
class ExecSuccess:
    module: str = field(default=None)
    fn: str = field(default=None)
    result: bool = field(default=False)


def whoami():
    """Return a function's name.

    Example:

    def foo():
        print(whoami())

    foo()

    ## $ whoami
    """
    import inspect

    frame = inspect.currentframe()
    return inspect.getframeinfo(frame).function


@dataclass
class CustomException:
    fn: str = field(default=None)
    msg: str = field(default=None)
    extra: Any = field(default=None)

    @property
    def exc_str(self) -> str:
        _except = f"{self.msg}. Details: {self.extra}"

        return _except

    @property
    def detail(self) -> dict:
        _detail = {"function": self.fn, "message": self.msg, "extra": self.extra}

        return _detail

    def _raise(self) -> None:
        raise Exception(self.exc_str)

    def _print(self) -> None:
        print(self.exc_str)
        print(f"Extra: {self.extra}")


def project_setup() -> True:
    _in: str = f"nb_starter.ipynb"
    _out: str = f"../{repo_name}.ipynb"

    print(f"Moving [{_in}] to [{_out}]")

    try:
        shutil.move(_in, _out)

        return True
    except Exception as exc:
        # raise Exception(
        #     f"Unhandled exception copying file: [{repo_name}/nb_starter.ipynb] -> [{repo_name}.ipynb].\nException details:\n{exc}"
        # )

        _exc = CustomException(
            fn=whoami(),
            msg=f"Unhandled exception copying file: [nb_starter.ipynb] -> [{repo_name}.ipynb].",
            extra=exc,
        )

        print(_exc.detail)

        # print(
        #     f"Unhandled exception copying file: [{repo_name}/nb_starter.ipynb] -> [{repo_name}.ipynb].\nException details:\n{exc}"
        # )


# def cleanup() -> bool:
#     try:
#         os.chdir("..")

#         shutil.rmtree(repo_name)

#         return True
#     except FileNotFoundError as fnf_exc:
#         pass
#     except Exception as exc:
#         # raise Exception(
#         #     f"Unhandled exception removing directory: {repo_name}. Details: {exc}"
#         # )

#         _exc = CustomException(
#             fn=whoami(),
#             msg=f"Unhandled exception removing directory: {repo_name}.",
#             extra=exc,
#         )

#         print(_exc.detail)


if __name__ == "__main__":
    _setup = project_setup()

    # _cleanup = cleanup()

    ## Remove empty folder cookiecutter created
    os.chdir("..")
    shutil.rmtree(repo_name)

    # Setup = ExecSuccess(
    #     **{"module": "main.py", "fn": "project_setup()", "result": _setup}
    # )
    # Cleanup = ExecSuccess(
    #     **{"module": "main.py", "fn": "cleanup()", "result": _cleanup}
    # )

    # exit_codes: list[ExecSuccess] = [Setup, Cleanup]

    # if Setup.result and Cleanup.result:
    #     print(f"Notebook creation [{repo_name}.ipynb] successful")

    # else:
    #     for _res in exit_codes:
    #         # print(f"Exit Code: {_res}")
    #         # print(f"Result type: {type(_res.result)}")
    #         # if not _res.result:
    #         #     print(f"[FAILED] {_res.module} -> {_res.fn}.")

    #         if not _res.result:
    #             print(f"[Failed]: {_res.module}:{_res.fn}")
