import subprocess
from pathlib import Path
import shutil

repo_name: str = "{{cookiecutter.repo_name}}"

def project_setup():
    """Create dir(s) for app."""
    
    ## List of directories to create, if they do not exist
    _dirs: list[str] = [
        f"{repo_name}"
    ]
    ## List of files to create, if they do not exist
    _files: list[str] = [
        f"{repo_name}/settings/env_files/.env",
        f"{repo_name}/settings/env_files/logging.env"
    ]
    
    ## Loop dirs
    for _d in _dirs:
        ## Create dir if not exists
        if not Path(_d).exists():
            Path(_d).mkdir(parents=True, exist_ok=True)
            
    ## Loop files
    for _f in _files:
        ## Check for presence of file
        if not Path(_f).exists():
            ## Set var for example file
            _f_ex: str = f"{_f}.example"
            
            if Path(_f_ex).exists():
                ## Copy example file to "real" file, if example exists
                try:
                    shutil.copyfile(_f_ex, _f)
                except Exception as exc:
                    raise Exception(f"Unhandled exception copying {_f_ex} to {_f}. Details: {exc}")
            else:
                print(f"Example file not found: {_f_ex}. Exiting.")
                exit(1)
      

def pdm_install():
    """Run pdm install command after project creation."""
    print(f"Running 'pdm install'")
    
    subprocess.run(["pdm", "install"])


if __name__ == "__main__":
    pdm_install()
    project_setup()
