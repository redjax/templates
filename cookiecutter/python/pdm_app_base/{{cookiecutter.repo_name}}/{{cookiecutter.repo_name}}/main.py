from settings.config import app_settings, logging_settings
from utils.logger import get_logger

log = get_logger(__name__, level=logging_settings.LOG_LEVEL)

from core.db import Base, get_engine, get_session, create_base_metadata


engine = get_engine(connection="db/demo.sqlite", echo=True)
create_base_metadata(base_obj=Base, engine=engine)
SessionLocal = get_session(engine=engine)


if __name__ == "__main__":
    log.info("Starting app")

    log.debug(f"App settings: {app_settings}")
